/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.ex2;

/**
 *
 * @author sala304b
 */
public class Carro {
    
     double distribuidora = 0.28 ; 
     double imposto = 0.45;
    
    public double calcular(double custo ){
        
        double Imposto  = custo * imposto ; 
        double lucro = custo * distribuidora ; 
        double preco = custo + Imposto + lucro ; 
        
        return  preco ; 
    }
    
}
